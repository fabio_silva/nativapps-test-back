package com.nativapps.test.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nativapps.test.domain.Curso;
import com.nativapps.test.repositories.CursoRepository;
import com.nativapps.test.utils.RESTfulUtil;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/cursos")
public class CursosController {

	@Autowired
	private CursoRepository repository;	
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Curso>> listAll() throws Exception {
		return new RESTfulUtil<Curso>().checkContentList(repository.findAll());
	}	
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Integer> create(@RequestBody Curso toSave) {
		return new RESTfulUtil<Curso>().create(toSave, repository);
	}

	@RequestMapping(method=RequestMethod.PUT)
	public ResponseEntity<HttpStatus> update(@RequestBody Curso toSave) throws Exception {
		return new RESTfulUtil<Curso>().update(toSave, repository);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> delete(@PathVariable("id") Integer id) throws Exception {	
		return new RESTfulUtil<Curso>().delete(id, repository);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public ResponseEntity<Curso> get(@PathVariable("id") Integer id) throws Exception {	
		return new RESTfulUtil<Curso>().checkContent(repository.findOne(id));
	}	
}
