package com.nativapps.test.controllers;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nativapps.test.domain.User;
import com.nativapps.test.repositories.UserRepository;
import com.nativapps.test.utils.Criptografia;
import com.nativapps.test.utils.CriptografiaEnum;
import com.nativapps.test.utils.HashEnum;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/auth")
public class AuthController {

	public static final String KEY_CRIPTOGRAFIA = "N1a8t4i0v3a0p5p4s";	
	
	@Autowired
	private UserRepository repository;	
	
	@PostConstruct
	public void config() throws Exception {
		User user = new User();
		user.setLogin("admin");
		user.setSenha(encriptaSenha(user.getLogin()));
		user.setPerfil(User.PERFIL_ADMIN);
		
		repository.save(user);
	}
	
    private String encriptaSenha(String senha) throws Exception {
        byte[] dados = Criptografia.encrypt(KEY_CRIPTOGRAFIA, senha, CriptografiaEnum.DESede);
        return Criptografia.hashToHex(new String(dados, "UTF-8"), HashEnum.SHA256);
    }	
    
    @RequestMapping(value="login", method=RequestMethod.POST)
    public ResponseEntity<HttpStatus> login(@RequestBody User user) throws Exception {
    		String senha = encriptaSenha(user.getSenha());
    		User saved = repository.findOneByLoginAndSenha(user.getLogin(), senha); 
    		
    		if (saved == null) {
    			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    		} else {
    			return new ResponseEntity<>(HttpStatus.OK);
    		}
    }
}
