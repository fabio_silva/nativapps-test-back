package com.nativapps.test.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@Data
@MappedSuperclass
public class PessoaDefault extends DomainDefault {

	private static final long serialVersionUID = 4462273225329207274L;
	
	private String identificacao; 
	@Column(nullable=false)
	private String sobrenome; 
	@Column(nullable=false, length=1)
	private String genero;
}
