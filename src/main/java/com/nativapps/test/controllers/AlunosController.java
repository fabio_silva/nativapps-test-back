package com.nativapps.test.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nativapps.test.domain.Aluno;
import com.nativapps.test.repositories.AlunoRepository;
import com.nativapps.test.repositories.CursoRepository;
import com.nativapps.test.utils.RESTfulUtil;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/alunos")
public class AlunosController {
	
	@Autowired
	private CursoRepository cursoRepository;		
	
	@Autowired
	private AlunoRepository repository;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Aluno>> listAll() throws Exception {
		return new RESTfulUtil<Aluno>().checkContentList(repository.findAll());
	}	
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Integer> create(@RequestBody Aluno toSave) {
		return new RESTfulUtil<Aluno>().create(toSave, repository);
	}

	@RequestMapping(method=RequestMethod.PUT)
	public ResponseEntity<HttpStatus> update(@RequestBody Aluno toSave) throws Exception {
		return new RESTfulUtil<Aluno>().update(toSave, repository);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> delete(@PathVariable("id") Integer id) throws Exception {	
		return new RESTfulUtil<Aluno>().delete(id, repository);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public ResponseEntity<Aluno> get(@PathVariable("id") Integer id) throws Exception {	
		return new RESTfulUtil<Aluno>().checkContent(repository.findOne(id));
	}
	
	@RequestMapping(value="{id}/cursos/{idCurso}", method=RequestMethod.POST)
	public ResponseEntity<HttpStatus> addCurso(@PathVariable("id") Integer id, @PathVariable("idCurso") Integer idCurso) throws Exception {	
		return new RESTfulUtil<Aluno>().addCurso(id, idCurso, repository, cursoRepository);
	}	
	
	@RequestMapping(value="{id}/cursos/{idCurso}", method=RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteCurso(@PathVariable("id") Integer id, @PathVariable("idCurso") Integer idCurso) throws Exception {	
		return new RESTfulUtil<Aluno>().deleteCurso(id, idCurso, repository, cursoRepository);
	}	
}
