package com.nativapps.test.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "professores")
public class Professor extends PessoaDefault implements IHasCursos {
	
	private static final long serialVersionUID = -6561230222457975286L;
	
    @ManyToMany
    @JoinTable(name="professores_cursos", joinColumns={@JoinColumn(name="id_professor")}, inverseJoinColumns={@JoinColumn(name="id_curso")})
    @OrderBy("nome ASC")
    private List<Curso> cursos = new ArrayList<>();	
}
