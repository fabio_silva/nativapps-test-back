package com.nativapps.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nativapps.test.domain.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	User findOneByLoginAndSenha(String login, String senha);
}
