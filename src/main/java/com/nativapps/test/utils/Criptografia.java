package com.nativapps.test.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;

public class Criptografia {
    
    public static String criptoMD5(String senha) throws Exception {
        MessageDigest strHash = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, strHash.digest(senha.getBytes("UTF-8")));
        String novaSenha = hash.toString(16);

        if (novaSenha.length() % 2 != 0) {
            novaSenha = "0" + novaSenha;
        }

        return novaSenha;
    }
    
    /** 
     * Encrypt / decrypt a String. 
     * @param encrypt true for encrypting, false for decrypting 
     * @param key the key should be at least 8 chars long (algorithm = DES or 24 when algorithm = DESede). Fill with 0 to left
     * @param text the text to encrypted / decrypted 
     * @param algorithm algorithm to encrypt / decrypt 
     * @return the decrypted / encrypted text 
     */  
    private static byte[] crypto(Boolean encrypt, String key, byte[] text, CriptografiaEnum algorithm) 
                                                                                    throws InvalidKeyException, NoSuchAlgorithmException, 
                                                                                    NoSuchPaddingException, InvalidKeySpecException, 
                                                                                    IllegalBlockSizeException, BadPaddingException, 
                                                                                    UnsupportedEncodingException {  
        KeySpec keySpec = null;
        
        Cipher cipher = Cipher.getInstance(algorithm.getAlgorithmName());  
        SecretKeyFactory factory = SecretKeyFactory.getInstance(algorithm.getAlgorithmName());  
        
        if (algorithm.equals(CriptografiaEnum.DES)) {
            key = StringUtils.leftPad(key, 8, "0");
            keySpec = new DESKeySpec(key.getBytes("UTF-8"));
        } else {
            key = StringUtils.leftPad(key, 24, "0");
            keySpec = new DESedeKeySpec(key.getBytes("UTF-8"));
        }
        
        cipher.init((encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE), factory.generateSecret(keySpec));
        
        return cipher.doFinal(text);  
    }  

    /** 
     * Decrypt a String. 
     * @param key the key should be at least 8 chars long (algorithm = DES or 24 when algorithm = DESede). Fill with 0 to left
     * @param text the text to encrypted / decrypted 
     * @param algorithm algorithm to encrypt / decrypt 
     * @return the decrypted / encrypted text 
     */  
    public static String decrypt(final String key, byte[] text, CriptografiaEnum algorithm) 
                                                                    throws InvalidKeyException, NoSuchAlgorithmException, 
                                                                    NoSuchPaddingException, InvalidKeySpecException, 
                                                                    IllegalBlockSizeException, BadPaddingException, 
                                                                    UnsupportedEncodingException {  
        return new String(crypto(Boolean.FALSE, key, text, algorithm), "UTF-8");  
    }     
    
    /** 
     * Encrypt a String. 
     * @param key the key should be at least 8 chars long (algorithm = DES or 24 when algorithm = DESede). Fill with 0 to left
     * @param text the text to encrypted / decrypted 
     * @param algorithm algorithm to encrypt / decrypt 
     * @return the decrypted / encrypted text 
     */  
    public static byte[] encrypt(final String key, final String text, CriptografiaEnum algorithm) 
                                                                    throws InvalidKeyException, NoSuchAlgorithmException, 
                                                                    NoSuchPaddingException, InvalidKeySpecException, 
                                                                    IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {  
        return crypto(Boolean.TRUE, key, text.getBytes("UTF-8"), algorithm);  
    }  

    public static byte[] hash(final String texto, HashEnum algorithmEnum) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest algorithm = MessageDigest.getInstance(algorithmEnum.getAlgorithmName());
        
        return algorithm.digest(texto.getBytes("UTF-8"));
    }
    
    public static String hashToHex(final String texto, HashEnum algorithmEnum) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return toHex(hash(texto, algorithmEnum));
    }
    
    public static byte[] hexToByte(final String texto) throws DecoderException {
        return Hex.decodeHex(texto.toCharArray());
    }
    
    public static String toHex(final byte[] texto) {
        return new String(Hex.encodeHex(texto));
    }
}