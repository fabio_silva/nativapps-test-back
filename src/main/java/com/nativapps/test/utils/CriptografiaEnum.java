package com.nativapps.test.utils;

public enum CriptografiaEnum {
	 
    DES("DES"),
    DESede("DESede");   // Triple-DES
    
    private final String algorithmName;
    
    CriptografiaEnum(final String algorithm) {
        this.algorithmName = algorithm;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }
}