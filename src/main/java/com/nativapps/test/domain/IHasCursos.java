package com.nativapps.test.domain;

import java.util.List;

public interface IHasCursos {
	
	List<Curso> getCursos();
	void setCursos(List<Curso> cursos);
}
