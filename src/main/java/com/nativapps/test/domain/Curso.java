package com.nativapps.test.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "cursos")
public class Curso extends DomainDefault {
	
	private static final long serialVersionUID = -6868461938849826773L;
	
	private String codigo;
	private String observacoes;
}
