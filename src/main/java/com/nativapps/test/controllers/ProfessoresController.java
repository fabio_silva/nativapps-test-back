package com.nativapps.test.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nativapps.test.domain.Professor;
import com.nativapps.test.repositories.CursoRepository;
import com.nativapps.test.repositories.ProfessorRepository;
import com.nativapps.test.utils.RESTfulUtil;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api/professores")
public class ProfessoresController {

	@Autowired
	private CursoRepository cursoRepository;	
	
	@Autowired
	private ProfessorRepository repository;	
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Professor>> listAll() throws Exception {
		return new RESTfulUtil<Professor>().checkContentList(repository.findAll());
	}	
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Integer> create(@RequestBody Professor toSave) {
		return new RESTfulUtil<Professor>().create(toSave, repository);
	}

	@RequestMapping(method=RequestMethod.PUT)
	public ResponseEntity<HttpStatus> update(@RequestBody Professor toSave) throws Exception {
		return new RESTfulUtil<Professor>().update(toSave, repository);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> delete(@PathVariable("id") Integer id) throws Exception {	
		return new RESTfulUtil<Professor>().delete(id, repository);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public ResponseEntity<Professor> get(@PathVariable("id") Integer id) throws Exception {	
		return new RESTfulUtil<Professor>().checkContent(repository.findOne(id));
	}	
	
	@RequestMapping(value="{id}/cursos/{idCurso}", method=RequestMethod.POST)
	public ResponseEntity<HttpStatus> addCurso(@PathVariable("id") Integer id, @PathVariable("idCurso") Integer idCurso) throws Exception {	
		return new RESTfulUtil<Professor>().addCurso(id, idCurso, repository, cursoRepository);
	}		
	
	@RequestMapping(value="{id}/cursos/{idCurso}", method=RequestMethod.DELETE)
	public ResponseEntity<HttpStatus> deleteCurso(@PathVariable("id") Integer id, @PathVariable("idCurso") Integer idCurso) throws Exception {	
		return new RESTfulUtil<Professor>().deleteCurso(id, idCurso, repository, cursoRepository);
	}	
}
