package com.nativapps.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nativapps.test.domain.Professor;

public interface ProfessorRepository extends JpaRepository<Professor, Integer> {
}
