package com.nativapps.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nativapps.test.domain.Curso;

public interface CursoRepository extends JpaRepository<Curso, Integer> {
}
