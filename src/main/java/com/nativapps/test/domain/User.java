package com.nativapps.test.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class User {

	public static final int PERFIL_ADMIN = (int) Math.pow(2, 0);
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String login;
	@Column(nullable=false)
	private String senha;
	@Column(nullable=false)
	private int perfil;
}
