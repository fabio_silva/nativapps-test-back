package com.nativapps.test.utils;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.nativapps.test.domain.Curso;
import com.nativapps.test.domain.DomainDefault;
import com.nativapps.test.domain.IHasCursos;
import com.nativapps.test.repositories.CursoRepository;

public class RESTfulUtil<T extends DomainDefault> {
	
	public ResponseEntity<HttpStatus> addCurso(Integer id, Integer idCurso, JpaRepository<T, Integer> repository, CursoRepository cursoRepository) {
		return addDeleteCurso(id, idCurso, repository, cursoRepository, Boolean.TRUE); 
	}
	
	public ResponseEntity<HttpStatus> addDeleteCurso(Integer id, Integer idCurso, JpaRepository<T, Integer> repository, CursoRepository cursoRepository, Boolean isAdd) {
		T saved = repository.findOne(id);
		if (saved == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Curso curso = cursoRepository.findOne(idCurso);
		if (curso == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		IHasCursos hasCursos = ((IHasCursos) saved);
		List<Curso> cursos = hasCursos.getCursos();
		
		if (Boolean.TRUE.equals(isAdd)) {
			if (!cursos.contains(curso)) {
				cursos.add(curso);
				repository.save(saved);
			}
		} else {
			if (cursos.contains(curso)) {
				cursos = cursos.stream()
								.filter(item -> item.getId().intValue() != idCurso)
								.collect(Collectors.toList());
				hasCursos.setCursos(cursos);
				repository.save(saved);
			}
		}
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);		
	}
	
	public ResponseEntity<T> checkContent(T entity) {
		if (entity == null) {
			return new ResponseEntity<T>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<T>(entity, HttpStatus.OK);
		}			
	}
	
	public ResponseEntity<List<T>> checkContentList(List<T> list) {
		if (list == null || list.isEmpty()) {
			return new ResponseEntity<List<T>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<T>>(list, HttpStatus.OK);
		}			
	}
	
	public ResponseEntity<Integer> create(T toSave, JpaRepository<T, Integer> repository) {
		repository.save(toSave);
		return new ResponseEntity<Integer>(toSave.getId(), HttpStatus.CREATED);
	}
	
	public ResponseEntity<HttpStatus> delete(Integer id, JpaRepository<T, Integer> repository) throws Exception {
		try {
			repository.delete(id);
		} catch (Exception e) { 
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} 	
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	public ResponseEntity<HttpStatus> deleteCurso(Integer id, Integer idCurso, JpaRepository<T, Integer> repository, CursoRepository cursoRepository) {
		return addDeleteCurso(id, idCurso, repository, cursoRepository, Boolean.FALSE);	
	}	
	
	public ResponseEntity<HttpStatus> update(T entity, JpaRepository<T, Integer> repository) throws Exception {
		T saved = repository.findOne(entity.getId());
		if (saved == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		repository.save(entity);		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
