package com.nativapps.test.utils;

public enum HashEnum {
	 
    MD5("MD5"),
    SHA1("SHA-1"),
    SHA256("SHA-256");
    
    private final String algorithmName;
    
    HashEnum(final String algorithm) {
        this.algorithmName = algorithm;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }
}