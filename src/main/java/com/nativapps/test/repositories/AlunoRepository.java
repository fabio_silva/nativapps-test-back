package com.nativapps.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nativapps.test.domain.Aluno;

public interface AlunoRepository extends JpaRepository<Aluno, Integer> {
}
